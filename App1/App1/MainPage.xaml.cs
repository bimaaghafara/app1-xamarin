﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App1
{
    public class UserAuthDto
    {
        public string UserId { get; set; }
        public string Password { get; set; }
        public UserAuthDto(string userId, string password)
        {
            UserId = userId;
            Password = password;
        }
    }

    public class UserAuthResultDto
    {
        public bool IsAuthorized { get; set; }
        public string Token { get; set; }
    }

    public partial class MainPage : ContentPage
    {
        protected string newButtonText = "new button!";
        protected string WebApi = "http://rnd.infolog.com.sg:81/WebAPI/api/";

    //Constructor
    public MainPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            //custom component
            messageLabel.Text = null;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }


        async void OnLoginAsync(object sender, EventArgs e)
        {
            UserAuthDto userAuth = new UserAuthDto(editorUserId.Text, editorPassword.Text);
            Debug.WriteLine("doLogin");
            Debug.WriteLine(editorUserId.Text);
            Debug.WriteLine(editorPassword.Text);
            buttonLogin.IsEnabled = false;
            messageLabel.Text = "Loading . . .";
            await doLogin(userAuth);
            buttonLogin.IsEnabled = true;
        }

        void OnForgot(object sender, EventArgs e)
        {
            Debug.WriteLine("OnForgot!");
        }

        public async Task doLogin(UserAuthDto userAuth)
        {
            var authUrl = WebApi + "auth";
            var uri = new Uri(string.Format(authUrl, string.Empty));
            var jsonPost = JsonConvert.SerializeObject(userAuth);
            var content = new StringContent(jsonPost, Encoding.UTF8, "application/json");
            var client = new HttpClient();
            var response = await client.PostAsync(uri, content);
            var jsonResponse = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<UserAuthResultDto>(jsonResponse);
            try
            {
                var IsAuthorized = result.IsAuthorized;
                Debug.WriteLine(IsAuthorized);
                Debug.WriteLine("success login");
                messageLabel.Text = "Success login!";
            }
            catch (Exception e)
            {
                Debug.WriteLine("fail login");
                Debug.WriteLine(e);
                messageLabel.Text = "Fail login!";
            }
            //var isa = result.IsAuthorized;
            //return (new { isaa = isa });
        }
    }
}